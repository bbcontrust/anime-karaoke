# Anime Karaoke

Project for setup karaoke quiz, randomize anime list of players and serv them lyrics.

## Project setup
```
npm install
copy .env .env.local
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
