import { makeId } from '@/tools/helpers'
import { anidb, anilist, kitsu, mal, malApi } from '@/tools/parsers'
import toastr from 'toastr'
import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)
const LOCAL_STORAGE_KEY = 'random-anime-lists'

export default new Vuex.Store({
  state: {
    lists: localStorage.getItem(LOCAL_STORAGE_KEY) ? JSON.parse(localStorage.getItem(LOCAL_STORAGE_KEY)) : [],
  },
  mutations: {
    ['renameList'] (state, {name, list}) {
      if (list.length < 50) {
        toastr.warning('List has less than 50 records')
      }
      state.lists = state.lists.map(item => {
        return list.id !== item.id ? item : {...item, name}
      })
      localStorage.setItem(LOCAL_STORAGE_KEY, JSON.stringify(state.lists))
    },
    ['removeList'] (state, list) {
      state.lists = state.lists.filter(({id}) => id !== list.id)
      localStorage.setItem(LOCAL_STORAGE_KEY, JSON.stringify(state.lists))
    },
    ['addList'] (state, list) {
      state.lists.push(list)
      localStorage.setItem(LOCAL_STORAGE_KEY, JSON.stringify(state.lists))
    }
  },
  actions: {
    async ['import.mal.api'] ({commit}, name) {
      const list = await malApi(name)
      if (list) {
        commit('addList', {id: makeId(12), ...list})
      }
    },
    async ['import.mal'] ({commit}, files) {
      const list = await mal(files)
      if (list) {
        commit('addList', {id: makeId(12), ...list})
      }
    },
    async ['import.al'] ({commit}, name) {
      const list = await anilist(name)
      if (list) {
        commit('addList', {id: makeId(12), ...list})
      }
    },
    async ['import.anidb'] ({commit}, file) {
      const list = await anidb(file)
      if (list) {
        commit('addList', {id: makeId(12), ...list})
      }
    },
    async ['import.kitsu'] ({commit}, name) {
      const list = await kitsu(name)
      if (list) {
        commit('addList', {id: makeId(12), ...list})
      }
    },
  },
  modules: {}
})
